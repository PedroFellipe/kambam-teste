@extends('layouts.app')
@section('title')
    Lista de Tarefas
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> To do </th>
                                  <th> Doing </th>
                                  <th> Review </th>
                                  <th> Done </th>
                              </tr>
                          </thead>
                          <tbody>
                                <tr>
                                    <td>
                                      @foreach($tasks1 as $task)
                                      <table class="table">
                                          <td>
                                            <a href= "{!! route('TaskAtt',['task' => $task->id],['project' => $project->id]) !!}"><i class="glyphicon glyphicon-ok"></i></a>  {!! $task->title !!}
                                          </td>
                                      </table>
                                      @endforeach
                                    </td>
                                    <td>
                                      @foreach($tasks2 as $task)
                                      <table class="table">
                                          <td>
                                            <a href= "{!! route('TaskAttB',['task' => $task->id],['project' => $project->id]) !!}"><i class="glyphicon glyphicon-arrow-left"></i></a>  <a href= "{!! route('TaskAtt',['task' => $task->id],['project' => $project->id]) !!}"><i class="glyphicon glyphicon-ok"></i></a> {!! $task->title !!}
                                          </td>
                                      </table>
                                      @endforeach
                                    </td>
                                    <td>
                                      @foreach($tasks3 as $task)
                                      <table class="table">
                                          <td>
                                            <a href= "{!! route('TaskAttB',['task' => $task->id],['project' => $project->id]) !!}"><i class="glyphicon glyphicon-arrow-left"></i></a>  <a href= "{!! route('TaskAtt',['task' => $task->id],['project' => $project->id]) !!}"><i class="glyphicon glyphicon-ok"></i></a> {!! $task->title !!}
                                          </td>
                                      </table>
                                      @endforeach
                                    </td>
                                    <td>
                                      @foreach($tasks4 as $task)
                                      <table class="table">
                                          <td>
                                            <a href= "{!! route('TaskAttB',['task' => $task->id],['project' => $project->id]) !!}"><i class="glyphicon glyphicon-arrow-left"></i></a> {!! $task->title !!}
                                          </td>
                                      </table>
                                      @endforeach
                                    </td>
                                </tr>
                          </tbody>
                      </table>
                    </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection
