@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><center>Bem vindo ao seu quadro kanbam =D </center></div>

                <div class="panel-body">
                    <center><a class="btn btn-success"  href="/project/"><i class="glyphicon"></i> Visualizar seus projetos</a></center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
