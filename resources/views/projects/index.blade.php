@extends('layouts.app')
@section('title')
    Projetos
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                      <table class="table">
                          <thead>
                              <tr>
                                <th> Projetos </th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach($projects as $project)
                                  <tr>
                                      <td>
                                        <div class="panel-heading">
                                          <h4>{!! $project->title !!}</h4>
                                          <div class="btn-group btn-group-sm">
                                              <a type="button" href= "{!! route('TaskIndex', ['project' => $project->id]) !!}" class="btn btn-primary"><i class="glyphicon glyphicon-play"></i> Visualizar tarefas</a>
                                              <a type="button" href= "{!! route('ProjectEdit', ['project' => $project->id]) !!}" class="btn btn-warning"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
                                              <a type="button" href= "{!! route('ProjectDelete', ['project' => $project->id]) !!}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Deletar</a>
                                          </div>
                                       </div>
                                      </td>
                                  </tr>
                              @endforeach
                          </tbody>
                      </table>
                    <a class="btn btn-success"  href="/project/create"><i class="glyphicon glyphicon-plus"></i> Criar Novo Projeto</a>
               </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    {{ HTML::script('src/js/sweetalert.min.js') }}
@endsection
