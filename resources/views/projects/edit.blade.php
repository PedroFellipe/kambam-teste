@extends('layouts.app')
@section('title')
    Editar projeto
@endsection
@section('content')
    <div class="container-fluid">
        <div class="col-md-8 col-md-offset-2">
            <h1> Editar projeto </h1>
            {!! Form::open(array('url' => "project/{$project->id}", 'method' => 'PUT')) !!}
            {!! Form::label('title', 'Projeto') !!}
            {!! Form::text('title', $project->title, array('class'=>'form-control')) !!}
            {!! Form::input('hidden', 'idCreator', Auth::user()->id, array('class'=>'form-control') ) !!}

            {!! Form::submit('Salvar Projeto', array('class'=>'btn btn-sucess btn-lg btn-block')) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
