@extends('layouts.app')
@section('title')
    Criar nova tarefa
@endsection
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1> Criar novo projeto </h1>
            {!! Form::open(array('url' => 'project', 'method' => 'POST')) !!}
            {!! Form::label('title', 'Projeto') !!}
            {!! Form::text('title', null, array('class'=>'form-control')) !!}
            {!! Form::input('hidden', 'idCreator', Auth::user()->id, array('class'=>'form-control') ) !!}
            
            {!! Form::submit('Criar novo projeto', array('class'=>'btn btn-primary')) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
