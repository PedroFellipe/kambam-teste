<!DOCTYPE html>
<html lang="pt">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=devic-width, initial-scale=1">
     <meta name="csrf-token" content={{ csrf_token() }}>
    {{ HTML::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}
    {{ HTML::style('src/css/sweetalert.css') }}
    {{ HTML::style('src/css/wrapper.css') }}
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
    {{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js') }}

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">

            <a class="navbar-brand" href="/">Início</a>
            <a class="navbar-brand" href="/task">Tarefas</a>
            <a class="navbar-brand" href="/task/create">Criar Nova Tarefa</a>
            <a class="navbar-brand" href="#">About</a>
        </div>
        <ul class="nav navbar-nav">

        </ul>
    </div>
</nav>
<div class="container">
    @yield('content')
</div>
<footer class="modal-footer">
    <address> UEMA - São Luís - Maranhão</address>

</footer>
@section('scripts')
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
    {{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js') }}
    {{ HTML::script('src/js/sweetalert.min.js') }}
@show
</body>
</html>
