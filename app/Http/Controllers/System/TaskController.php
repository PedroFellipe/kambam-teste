<?php

namespace App\Http\Controllers\System;

use App\User;
use App\Model\Project;
use App\Model\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
      * Create a new controller instance.
      *
      * @return void
      */
     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index($ProjectId)
    {
       $tasks1 = DB::table('tasks')
                    ->where('idProject', '=', $ProjectId)
                    ->where('status','=','todo')
                    ->get();
       $tasks2 = DB::table('tasks')
                    ->where('idProject', '=', $ProjectId)
                    ->where('status','=','doing')
                    ->get();
       $tasks3 = DB::table('tasks')
                    ->where('idProject', '=', $ProjectId)
                    ->where('status','=','review')
                    ->get();
       $tasks4 = DB::table('tasks')
                    ->where('idProject', '=', $ProjectId)
                    ->where('status','=','done')
                    ->get();
      $project = Project::find($ProjectId);


        //$users = User::where('id', Auth::user()->id);;
        //$user = DB::table('users')->where('id', Auth::user()->id)->first();
        return view('tasks.index', compact('tasks1','tasks2','tasks3','tasks4','project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
        // Validação
        /*$this->validate($request, array(
            'mensagem' => 'required|max:400',
            'idCreator' => 'required'
        ));*/

        //dd($request);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {

     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //


    }


    public function attStatus(Request $request, $id)
    {
        $task = Task::find($id);
        //if($task == null) return redirect('task');
        if($task->status === "todo"){
            $task->status = "doing";
            $task->save();
            return redirect()->back();
        }
       if($task->status === "doing"){
           $task->status = "review";
            $task->save();
            return redirect()->back();
        }
      if($task->status === "review"){
          $task->status = "done";
          $task->save();
          return redirect()->back();
        }

   }

   public function attStatusB(Request $request, $id)
   {
       $task = Task::find($id);
       //if($task == null) return redirect('task');
       if($task->status === "done"){
           $task->status = "review";
           $task->save();
           return redirect()->back();
       }
      if($task->status === "review"){
          $task->status = "doing";
           $task->save();
           return redirect()->back();
       }
     if($task->status === "doing"){
         $task->status = "todo";
         $task->save();
         return redirect()->back();
       }

  }

}
