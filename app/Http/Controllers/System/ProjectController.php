<?php

namespace App\Http\Controllers\System;

use App\User;
use App\Model\Task;
use App\Model\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
      * Create a new controller instance.
      *
      * @return void
      */
     public function __construct()
     {
         $this->middleware('auth');
     }

    public function getIndex()
    {
        $projects = Project::
          where('idCreator', Auth::user()->id)->paginate(7);
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
        // Validação
        $this->validate($request, array(
            'title' => 'required|max:400',
            'idCreator' => 'required'
        ));

        $project = new Project;
        $project->title = $request->title;
        $project->idCreator = $request->idCreator;

        $project->save();

        return redirect('project');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        //
        $project=Project::find($id);
        return view('projects.edit',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function putEdit(Request $request, $id)
     {
         // Validação
         // Validação
         $this->validate($request, array(
             'title' => 'required|max:400',
             'idCreator' => 'required',
         ));

         $project = Project::find($id);
         $project->title = $request->title;
         $project->idCreator = $request->idCreator;
         $project->save();
         return redirect('project');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDelete($id)
    {
        $project = Project::find($id);
        $project->delete();
        return redirect('project');
    }
}
