<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group( ['namespace' => 'System'], function (){
                Route::get('/project', 'ProjectController@getIndex');
                Route::get('/project/create','ProjectController@getcreate');
                Route::post('/project','ProjectController@postCreate');
                Route::get('/project/{project}/edit', 'ProjectController@getEdit')->name('ProjectEdit');
                Route::put('/project/{project}','ProjectController@putEdit');
                Route::get('project/{project}','ProjectController@getDelete')->name('ProjectDelete');
                Route::post('/project/{project}', 'ProjectController@postDelete');
                Route::get('/tarefa/{task}', 'TaskController@attStatus')->name('TaskAtt');
                Route::get('/tarefab/{task}', 'TaskController@attStatusB')->name('TaskAttB');
});


Route::group(['prefix' => '/{project}/task', 'namespace' => 'System'], function (){

                Route::get('/', 'TaskController@index')->name('TaskIndex');
                Route::get('/create', 'TaskController@create');
                Route::post('/', 'TaskController@store');
                Route::put('/{task}', 'TaskController@update');
                Route::post('/{task}', 'TaskController@show');
                Route::delete('/{task}', 'TaskController@destroy');
                Route::post('/{task}/edit', 'TaskController@edit');
});

Route::auth();

Route::get('/home', 'HomeController@index');
