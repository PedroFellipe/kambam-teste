<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

  protected $fillable = ['title'];

  public function creator()
    {
        return $this->belongsTo('App\User', 'foreign_key');
    }

  public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
